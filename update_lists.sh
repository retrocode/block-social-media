#!/usr/bin/env bash
# Just kill the script if there was a problem part-way
set -e

echo "Fetching latest block lists from StevenBlack/hosts..."
curl --silent 'https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/social/hosts' > source_hosts.txt

awk '/^# Title: Social media hosts/ { matched = 1 } matched' source_hosts.txt > all_social_media.txt

echo
echo "Displaying available lists..."
grep -P '^# ' all_social_media.txt

echo
awk '/^# Facebook/,/^$/' source_hosts.txt > facebook.txt
awk '/^# Instagram/,/^$/' source_hosts.txt > instagram.txt
awk '/^# Whatsapp/,/^$/' source_hosts.txt > whatsapp.txt
awk '/^# Twitter/,/^$/' source_hosts.txt > twitter.txt
awk '/^# LinkedIn/,/^$/' source_hosts.txt > linkedin.txt
awk '/^# MySpace/,/^$/' source_hosts.txt > myspace.txt
awk '/^# Pinterest/,/^$/' source_hosts.txt > pinterest.txt
awk '/^# Tumblr/,/^$/' source_hosts.txt > tumblr.txt
awk '/^# Reddit/,/^$/' source_hosts.txt > reddit.txt
awk '/^# Tik-Toc/,/^$/' source_hosts.txt > tiktok.txt

cat facebook.txt instagram.txt whatsapp.txt linkedin.txt myspace.txt pinterest.txt tumblr.txt tiktok.txt > allow_only_twitter_and_reddit.txt

echo
echo "Completed extraction"
