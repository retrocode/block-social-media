# Block Social Media

My blocklist for social media to be used for Pi-Hole (or simply on your local machine's hosts file), extracted from:

https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/social/hosts

This is a very simple script to fetch the latest block lists, and split them to separate files for more fine-grained control, if you still would like to use specific social media.

Sample link to the hosts file blocking all (included) social media except Reddit and Twitter:

https://gitlab.com/retrocode/block-social-media/-/raw/master/allow_only_twitter_and_reddit.txt